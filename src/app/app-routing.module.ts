import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './components/auth/auth.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ColorComponent } from './components/master/color/color.component';
import { MyAccountComponent } from './components/my-account/my-account.component';
import { ProductComponent} from './components/product/product.component'
import { SellerComponent } from './components/seller/seller.component';
import { SubscriptionPlanComponent } from './components/subscription-plan/subscription-plan.component';
import { DefaultComponent } from './layouts/default/default.component';
import { FullwidthComponent } from './layouts/fullwidth/fullwidth.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch:'full'},
  {
    path : '',
    component : FullwidthComponent,
    children : [{
      path:'login',
      component : AuthComponent,
    }]
  },
  {
    path : '',
    component : DefaultComponent,
    children : [{
      path : 'dashboard',
      component : DashboardComponent
     } ,
     {
       path : 'product',
       component : ProductComponent
     },
     {
       path : 'seller',
       component : SellerComponent
     },
     {
       path : 'myAccount',
       component : MyAccountComponent
     },
     {
        path: 'subscription-plan',
        component : SubscriptionPlanComponent
     },{
       path : 'master-color',
       component : ColorComponent
     }
    ]
}

  /* { path: '', redirectTo: 'login', pathMatch:'full'},
  { path : 'dashboard/:username' , component : DashboardComponent},
  { path : 'product' , component : ProductComponent},
  { path : 'login' , component : AuthComponent},
  { path : 'myAccount' , component : MyAccountComponent} */
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
