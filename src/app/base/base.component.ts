import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import {ProductService } from '../Services/productService/product.service';
import {MasterService} from '../Services/Master/master.service';

import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { RootService } from '../Services/root.service';
@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {

  constructor(
    public activateRoute : ActivatedRoute,
    public router: Router,
    public spinner: NgxSpinnerService,

    /**Custom services */
    public productService: ProductService,
    public masterServices : MasterService,
    public rootServices: RootService
  ) { }
  /** Master colors */
  tempImage: any;
  masterColors : any =[];
  showOverlay = false;
  tempImageUploadData : any;
  reader = new FileReader();
  ngOnInit(): void {
  }


  /**
   * Custom log
   */
  log(data){
    console.log(data);
  }


  /**Get all master colors */
  getMasterColors(){
    try {
      this.masterServices.getMasterColors().subscribe(
        res =>{
          if(res.statusCode = 200){
            this.masterColors = res.data[0]
          }
        }
      )
    } catch (error) {
      
    }
  }



/** get random images */
  getRandomImages(images){
      let noImage = 'https://thumbs.dreamstime.com/b/no-image-available-icon-flat-vector-no-image-available-icon-flat-vector-illustration-132482953.jpg';

      //console.log(images)
      if(images.length > 0 ){
        // setInterval(function(){ 
          let randomNo = Math.floor(Math.random() * (images.length)); 
         // console.log(randomNo)
          return images[0]['thumb_image'];
        // }, 10000);
      } else {
        return noImage;
      }
  }



  
  /**
   * End of classs
   */
}
