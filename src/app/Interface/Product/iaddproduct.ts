export interface IAddproduct {
    "name" : string;
    "model_no" : string;
    "description" : string;
    "material" : string;
    "catagory" : string;
    "type" : string;
    "brand" : string;
    "age_max" : string;
    "age_min" : string;
    "occation": string;
}

export interface Iaddsize
{
  "size" : string;
  "length" : string;
}

export interface Iaddcolor
{
  "colorname" : string;
  "colorimagepath" : string;
}

export interface IAddProductImages{
  productID: string,
  
}

export interface IAddProductImage {
  
}


/** Adding master colors */

export interface IAddMasterColor{
  "name" :string,
	"color_image": string,
  "color_image_thumb" : string
}