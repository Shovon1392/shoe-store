import { Component,OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';
import Amplify, { Auth, Storage } from 'aws-amplify';
import {
	Iaddcolor,
	IAddproduct,
	Iaddsize
} from '../../Interface/Product/iaddproduct';
import { environment } from 'src/environments/environment';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
	selector: 'app-product',
	templateUrl: './product.component.html',
	styleUrls: ['./product.component.scss']
})
export class ProductComponent extends BaseComponent implements OnInit {
	b64toBlob = require('b64-to-blob');
	contentType = 'image/jpg';
	addProductData: IAddproduct = {
		"name": null,
		"model_no": null,
		"description": null,
		"material": null,
		"catagory": null,
		"type": null,
		"brand": null,
		"age_max": null,
		"age_min": null,
		"occation": null
	}
  tempImage: any;
  tempImageString : any;
  tempUploadImage : any = [];
	productSize: Iaddsize[] = [{
		"size": null,
		"length": null
	}];
	productColor: Iaddcolor[] = [{
		"colorname": null,
		"colorimagepath": null
	}];
	allProduct: any = []; /**TO show the list of all product */
	selectedProduct: any = [];
  selectedColors: any = [];
  selectedColor = null /** IT is used to upload image according to product color */
	colorIndex = 0;
	imageChangedEvent: any = '';
    croppedImage: any = '';

    
	ngOnInit(): void {
    
		this.getAllProduct();
		/** Load master colors */
		if (this.masterColors.length == 0) {
			this.getMasterColors();
		}
	}
	fileChangeEvent(event: any): void {
		this.imageChangedEvent = event;
		console.log(event)
    }
    imageCropped(event: ImageCroppedEvent) {
        this.tempImageString = event.base64;
    }
    imageLoaded(image: HTMLImageElement) {
        // show cropper
    }
    cropperReady() {
        // cropper ready
    }
    loadImageFailed() {
        // show message
    }
	/* Add product size */
	addProductSize() {
		this.productSize.push({
			"size": null,
			"length": null
		})
	}
	/* Delete product size */
	deleteProductSize(plist) {
		this.productSize.splice(this.productSize.indexOf(plist), 1);
	}
	/* Add product color */
	addProductColor() {
		let color = this.masterColors[this.colorIndex]
		console.log(color)
		// this.productColor.push(
		//     { "colorname" : null,
		//        "colorimagepath" : null
		//     })
		this.selectedColors.push(color)
	}
	/* Delete product color */
	deleteProductColor(clist) {
		this.selectedColors.splice(this.selectedColors.indexOf(clist), 1);
	}
	/**
	 * GEt all products
	 */
	getAllProduct() {
		try {
			this.allProduct = [];
			this.spinner.show()
			this.productService.getAllMasterProduct().subscribe(res => {
				this.spinner.hide();
				if (res['data'][0].length > 0) {
					res['data'][0].forEach(element => {
						let tempdata = (element);
						tempdata.colors = (tempdata.colors) ? JSON.parse("[" + JSON.parse(tempdata.colors)[0] + "]") : [];
						tempdata.sizes = (tempdata.sizes) ? JSON.parse("[" + JSON.parse(tempdata.sizes) + "]") : [];
						tempdata.images = (tempdata.images) ? JSON.parse("[" + JSON.parse(tempdata.images) + "]") : [];
						this.allProduct.push(tempdata);
					});
					console.log(this.allProduct)
				}
			}, err => {
				this.spinner.hide();
				console.log(err)
			})
		} catch (error) {
			this.spinner.hide();
		}
	}
	/**Adding new product  */
	addProduct() {
		this.log(this.addProductData)
		if (this.addProductData.name) {
			try {
				this.spinner.show();
				//document.getElementById('modalClose').click();
				this.productService.addMasterProduct(this.addProductData).subscribe(res => {
					this.spinner.hide();
					this.getAllProduct();
					this.log(res)
				}, err => {
					this.spinner.hide();
					this.log(err)
				})
			} catch (error) {
				this.log(error)
			}
		}
	}
	/**Set selected product */
	selectProduct(product) {
		this.selectedProduct = product;
		
	}
	/** Save size */
	saveSize() {
		console.log(this.selectedProduct, this.productSize)
		if (this.productSize.length > 0) {
			let sizeAddData = {
				'producti_id': this.selectedProduct['productId'],
				'sizes': this.productSize
			}
			try {
				this.spinner.show();
				//document.getElementById('modalClose').click();
				this.productService.addProductSize(sizeAddData).subscribe(res => {
					this.spinner.hide();
					this.getAllProduct();
					this.log(res)
				}, err => {
					this.spinner.hide();
					this.log(err)
				})
			} catch (error) {
				this.log(error)
			}
		}
	}
	/** Set product colors */
	saveProductColors() {
		let tempColors = [];
		this.selectedColors.forEach(element => {
			tempColors.push({
				'color_id': element.hash_id
			})
		});
		let caolorAddData = {
			'producti_id': this.selectedProduct['productId'],
			'colors': tempColors
		}
		try {
			this.spinner.show();
			//document.getElementById('modalClose').click();
			this.productService.addProductColor(caolorAddData).subscribe(res => {
				this.spinner.hide();
				this.getAllProduct();
				this.log(res)
			}, err => {
				this.spinner.hide();
				this.log(err)
			})
		} catch (error) {
			this.log(error)
		}
	}
	/**
	 * upload image 
	 * 
	 */
	async uploadImage() {
	
	// 	this.reader.readAsDataURL(image[0]);
	// 	this.reader.onload = () => {
	// 		this.tempImage = this.reader.result;
    // }
    //console.log(this.selectedProduct)
    //this.rootServices.uploadImageTos3()
	let date= new Date();
	let blob = this.b64toBlob(this.tempImageString.split('base64,')[1], this.contentType);
	let blobUrl = URL.createObjectURL(blob);
    let path = this.selectedProduct['productId']+'/'+this.selectedProduct['productId']+'_product_'+date.getMilliseconds()+'.jpg'
	console.log(this.convertImage(this.tempImageString))
	this.showOverlay = true
    Storage.put(path,this.convertImage(this.tempImageString),{
	  bucket: environment.product_image_bucket,
      contentType: 'image/jpg',
      progressCallback(progress) {
        console.log(`Uploaded: ${progress.loaded}/${progress.total}`,progress);
      },
    }).then (result => 
      
      {
        this.tempUploadImage.push({
          product_image: 'https://'+environment.product_image_bucket+'.s3.amazonaws.com/public/'+result['key'],
          product_image_thumb: 'https://'+environment.product_thumb_image_bucket+'.s3.amazonaws.com/public/'+result['key']
        });
        this.showOverlay = false
        
        
        // return  this.tempImageUploadData ;
        //console.log('upload image path',resul t)
    })
    
  }
  

  /** upload and save product images */
  uploadProductImage(){
    let uploadData = {
      product_color_id : this.selectedColor,
      images : this.tempUploadImage
    }

    try {
      this.spinner.show();
      this.productService.addProductImages(uploadData).subscribe(
      res=> {
        this.spinner.hide();
				this.getAllProduct();
				this.log(res)
			}, err => {
				this.spinner.hide();
				this.log(err)
			})
		} catch (error) {
			this.log(error)
		}
    
  }

convertImage(dataURI) {

    var byteString = atob(dataURI.split(',')[1]);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);

    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { 
		
		type: 'image/jpeg',
		
	});
}
	/** End of class */
}