import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-seller',
  templateUrl: './seller.component.html',
  styleUrls: ['./seller.component.scss']
})
export class SellerComponent implements OnInit {

  constructor() { }

  sellerFilter:string = 'All';

  ngOnInit(): void {
  }

  ApplySellerFilter(sfilter:any)
  {
     this.sellerFilter = sfilter;
  }

}
