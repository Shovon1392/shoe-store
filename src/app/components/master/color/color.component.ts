import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';
import {  IAddMasterColor } from 'src/app/Interface/Product/iaddproduct';
import Amplify, { Auth, Storage } from 'aws-amplify';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-color',
  templateUrl: './color.component.html',
  styleUrls: ['./color.component.scss']
})
export class ColorComponent extends BaseComponent implements OnInit {

  addColorData: IAddMasterColor = {
    color_image: '',
    color_image_thumb: '',
    name: ''
  }
  ngOnInit(): void {
    if (this.masterColors.length == 0) {
      this.getMasterColors();
      
    }
    
  }
  /** creating  */
  addColor() {
    this.spinner.show();
    try {
      this.masterServices.createMasterColors(this.addColorData).subscribe(
        res =>{
          if(res.statusCode = 200){
            //this.masterColors = res.data[0];
            //console.log('jghhjjhgjjgjss', this.masterColors)
            this.getMasterColors();
            this.spinner.hide();
          }
        }
      )
    } catch (error) {
      
    }
  }
  /**
   * upload image 
   * 
   */
  async uploadImage(image) {
    this.reader.readAsDataURL(image[0]);
    this.reader.onload = () => {
      this.tempImage = this.reader.result;
    }
    let date = new Date();
    let path = 'master/color/master_color_' + date.toISOString() + '.' + image[0].name.split('.')[1]
    console.log(path)
    this.showOverlay = true
    Storage.put(path, image[0], {
      bucket: environment.product_master_bucket,
      contentType: 'image/jpg',
      progressCallback(progress) {
        console.log(`Uploaded: ${progress.loaded}/${progress.total}`, progress);
      },
    }).then(result => {
      this.addColorData.color_image = 'https://' + environment.product_master_bucket + '.s3.amazonaws.com/public/' + result['key'],
        this.addColorData.color_image_thumb = 'https://' + environment.product_master_bucket + '.s3.amazonaws.com/public/' + result['key'],
        this.showOverlay = false
    }).catch(error =>{
      console.log(error)
    })
  }

   /**Get all master colors */
   getMasterColors(){
    this.spinner.show();
    try {
      this.masterServices.getMasterColors().subscribe(
        res =>{
          if(res.statusCode = 200){
            this.masterColors = res.data[0];
            //console.log('jghhjjhgjjgjss', this.masterColors)
            this.spinner.hide();
          }
        }
      )
    } catch (error) {
      
    }
  }
  /**End of classs */
}