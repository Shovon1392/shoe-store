import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent extends BaseComponent implements OnInit {

  
/**
 * Go to next page 
 * 
 */
goNext(page){
  console.log(page)
  this.router.navigateByUrl(page)
}
  ngOnInit(): void {
  }



  /**
   * 
   * End of class
   */

}
