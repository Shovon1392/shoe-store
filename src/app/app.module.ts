import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BaseComponent } from './base/base.component';
import { FormsModule } from '@angular/forms';
import { NgxSpinnerModule } from "ngx-spinner";
import { DefaultModule } from './layouts/default/default.module';
import { FullwidthModule } from './layouts/fullwidth/fullwidth.module';
import { HttpClientModule } from '@angular/common/http';
import { SellerComponent } from './components/seller/seller.component';
import { SubscriptionPlanComponent } from './components/subscription-plan/subscription-plan.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ColorComponent } from './components/master/color/color.component';
 
import { ImageCropperModule } from 'ngx-image-cropper';

@NgModule({
  declarations: [
    AppComponent,
    BaseComponent,
    ColorComponent
    // SellerComponent,
    // SubscriptionPlanComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    DefaultModule,
    FullwidthModule,
    NgxSpinnerModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ImageCropperModule
    //NgxImageEditorModule.forRoot()
  ],
  providers: [
    //NgxImageEditorModule.forRoot()
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
