import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { timeout, catchError } from 'rxjs/operators';
import {RootService} from '../root.service';
import { IAddMasterColor } from 'src/app/Interface/Product/iaddproduct';

@Injectable({
  providedIn: 'root'
})
export class MasterService extends RootService {

  constructor(http: HttpClient){
    super(http);
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
       //'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers
    };
  }


/** create  master colors */
createMasterColors(data : IAddMasterColor): Observable<any>{
  return this.http.post(this.master_color, data, this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    );
}


/** Get master colors */
getMasterColors(): Observable<any>{
  return this.http.get(this.master_color , this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    );
}
  /** End of class */
}
