import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import {environment} from '../../environments/environment';
import Amplify, { Auth, Storage } from 'aws-amplify';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RootService {
  headers = new HttpHeaders({
    'Content-Type': 'application/json',
     //'Authorization': 'Bearer '+localStorage.getItem('token')
  });
  httpOptions = {
    headers: this.headers
  };

  constructor(
    public http: HttpClient
  ) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      //  'Authorization': 'Bearer '+ localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers
    };
  }
  timeOutLimit = 1000*15;

  /**
   * Base urls
   */

  base_product_url = 'https://jhk9yt94tl.execute-api.us-east-1.amazonaws.com/dev/api/v1/';
  base_master_url = 'https://7h6jh8qei0.execute-api.us-east-1.amazonaws.com/dev/api/v1/'
  /**
   * Product
   */

   master_product = this.base_product_url+'product/admin';
   product_size = this.base_product_url+'product/productSize';
   product_color = this.base_product_url+'product/productColor';
   product_image = this.base_product_url+'product/images';

   /** Master */
   master_color = this.base_master_url+'master/color';





   /** upload image to s3 bucket and return its path */

  uploadImageTos3(image,uploadFor,data): Observable<any> {
    let tempImageUploadData: any;
    console.log(image)
    let uploadData = this.getS3Bucket(image,uploadFor,data);
    //this.showOverlay = true;
   Storage.put(uploadData['name'],image,{
      bucket: uploadData['bucket'],
      contentType: 'image/jpg',
      progressCallback(progress) {
        console.log(`Uploaded: ${progress.loaded}/${progress.total}`,progress);
      },
    }).then (result => 
      
      {
        tempImageUploadData =  'https://'+uploadData['bucket']+'.s3.amazonaws.com/public/'+result['key'];
        return  tempImageUploadData;
        // return  this.tempImageUploadData ;
        //console.log('upload image path',result)
    })
      .catch(err => console.log(err));
      return  tempImageUploadData;
      //return
  }


  /**
   * Get bucket name for image upload
   */

   getS3Bucket(image,uploadFor,data){
     var returnData = {
       bucket: null,
       image_path : null
     }
     switch (uploadFor){

        /** Product image  */
       case 'product_image' : 
          let date= new Date();
          let path = data.productId
          returnData = {
            image_path : path+'/'+data.productId+'_'+date+'.'+image.name.split('.')[1],
            bucket : environment.product_image_bucket
          }
          return returnData;
     }
   }
  /**
   * End
   */
}
