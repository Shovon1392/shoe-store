import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { timeout, catchError } from 'rxjs/operators';
import {RootService} from '../root.service';
import { IAddproduct, Iaddsize } from 'src/app/Interface/Product/iaddproduct';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends RootService {

  constructor(http: HttpClient){
    super(http);
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
       //'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers
    };
  }



  /**
   * Get allmaster product list
   */
  getAllMasterProduct(): Observable<any>{
    return this.http.get(this.master_product,this.httpOptions).pipe(
        timeout(this.timeOutLimit)
      );
  }

  /**
   * Create new master Product
   */
  addMasterProduct(data: IAddproduct): Observable<any>{
    return this.http.post(this.master_product,data,this.httpOptions).pipe(
        timeout(this.timeOutLimit)
      );
  }


  /** Add product size */
  addProductSize(data): Observable<any>{
    return this.http.post(this.product_size,data,this.httpOptions).pipe(
        timeout(this.timeOutLimit)
      );
  }


  /**
   * Add product colors
   */
  addProductColor(data): Observable<any>{
    return this.http.post(this.product_color,data,this.httpOptions).pipe(
        timeout(this.timeOutLimit)
      );
  }


  /** Upload product images */
  addProductImages(data): Observable<any>{
    return this.http.post(this.product_image,data,this.httpOptions).pipe(
        timeout(this.timeOutLimit)
      );
  }
  /**
   * End of the class
   */
}
