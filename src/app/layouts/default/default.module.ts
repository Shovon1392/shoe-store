import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DefaultComponent} from './default.component'
import { DashboardComponent} from '../../components/dashboard/dashboard.component'
import { ProductComponent} from '../../components/product/product.component'
import { MyAccountComponent} from '../../components/my-account/my-account.component'
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgxSpinnerModule } from 'ngx-spinner';

import { ImageCropperModule } from 'ngx-image-cropper';

@NgModule({
  declarations: [
     DefaultComponent,
     DashboardComponent,
     ProductComponent,
     MyAccountComponent,
     

  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    FormsModule,
    BrowserModule,
    FormsModule,
    NgxSpinnerModule,
    ImageCropperModule
  ]
})
export class DefaultModule { }
