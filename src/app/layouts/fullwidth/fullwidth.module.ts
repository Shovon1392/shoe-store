import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FullwidthComponent } from './fullwidth.component'
import { AuthComponent} from '../../components/auth/auth.component'
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    FullwidthComponent,
    AuthComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule,
    NgxSpinnerModule
  ]
})
export class FullwidthModule { }
